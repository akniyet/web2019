$(function () {
    var calculator = $("#calculator");
    var functionButtons = "C*/-+=";
    var lines =[
        ["C"],
        ["*","/","-","+","="],
        ["9","8","7","6","0"],
        ["1","2","3","4","5"]
    ];
    var input = $("<div class='input'></div>");
    calculator.append(input);
    var equal, times, plus, minus, clear, slash;
    for (var i = 0; i<lines.length; i++) {
        var line = $("<div class='line'></div>");
        for (var j = 0; j < lines[i].length; j++) {
            var btn = $("<dvi class='button b'"+lines[i]
                [j]+"'></div>'");
            if (btn.text() == "+") plus = btn;
            else if (btn.text() == "-") minus = btn;
            else if (btn.text() == "*") times = btn;
            else if (btn.text() == "/") slash = btn;
            else if (btn.text() == "=") equal = btn;
            else if (btn.text() == "C") clear = btn;
            line.append(btn);
        }
        calculator.append(line);
    }
    window.onkeydown = function (event) {
        var e = event;
        console.log(e.keyCode);
        switch(e.keyCode) {
            case 67:
                clear.trigger("click");
                break;
            case 13:
                equal.trigger("click");
                break;
            case 187:
                if (e.shiftKey) plus.trigger("click");
                else equal.trigger("click");
                break;
            case 189:
                minus.trigger("click");
                break;
        }
    }
});
